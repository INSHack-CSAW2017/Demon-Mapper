## Installation

    sudo ./install.sh

## Test
Si vous n'avez pas de raspberry sous la main, vous pouvez tester via le mock:

    sudo ./install_mock.sh

## Utilisation du mock

    #Regarder les entrées
    watch -n 0 curl http://localhost:8085/get_inputs

    #Regarder les sorties
    watch -n 0 curl http://localhost:8085/get_outputs

    #Mettre une valeur (ici pin physique 2 à Faux)
    curl http://localhost:8085/set_input?pin=2&value=0

