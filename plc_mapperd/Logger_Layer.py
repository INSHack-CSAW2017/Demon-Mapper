import time
from liblogger import WriteInputLog,WriteOutputLog

rootFolder = "/var/log/gpioInOuts/"

buffInput = {}
buffOutput = {}
buffPlcOutput = {}

buffSize = 5

def log_inputs(group_id,inputs):
	timestamp = str(time.time())
	input = ""

	for c in inputs:
		if c: input +="1"
		else: input +="0"

	if group_id in buffInput.keys():

		#BuffSize is reached => Write
		if len(buffInput[group_id]) >= buffSize:
			WriteInputLog(rootFolder+"input_"+str(group_id)+".log\0",buffInput[group_id],len(buffInput[group_id]))
			buffInput[group_id] = []
	else:
		buffInput[group_id] = []

	buffInput[group_id].append(timestamp+":"+input)

def log_outputs(group_id,outputs):
	timestamp = str(time.time())

	output = ""

	int_out,bools = outputs
	for c in bools:
		if c:output+="1"
		else:output +="0"

	output+=":"+str(int_out)

	if group_id in buffOutput.keys():

		#BuffSize is reached => Write
		if len(buffOutput[group_id]) >= buffSize:
			WriteOutputLog(rootFolder+"output_"+str(group_id)+".log\0",buffOutput[group_id],len(buffOutput[group_id]))
			buffOutput[group_id] = []
	else:
		buffOutput[group_id] = []
	buffOutput[group_id].append(timestamp+":"+output)

def log_plc_output(group_id,identifier,output):
        timestamp = str(time.time())
        gid = str(group_id)+"_"+identifier

	#Convert
        final_int,final_bools = map(int,output.split(":"))
        bool_output = ""
        mask = 0b10000000000000
        for i in range(11):
            if final_bools&mask:bool_output+="1"
            else: bool_output+="0"
            mask = mask >> 1
        output = bool_output+":"+str(final_int)

        if gid in buffPlcOutput.keys():

                #BuffSize is reached => Write
                if len(buffPlcOutput[gid]) >= buffSize:
                        WriteOutputLog(rootFolder+"output_"+gid+".log\0",buffPlcOutput[gid],len(buffPlcOutput[gid]))
                        buffOutput[gid] = []
        else:

                buffPlcOutput[gid] = []

        buffPlcOutput[gid].append(timestamp+":"+output)

"""
log_inputs(1,[False for i in range(14)])
log_outputs(1,[[False for i in range(11)],1])
log_plc_output(1,"aaaa","0:0")
"""
