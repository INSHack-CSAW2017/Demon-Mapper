#!/usr/bin/python3.5
from ctypes import *
from time import *
IOlib=CDLL("/usr/sbin/plc_mapperd/IOlib.so")
def WriteInputLog(logfile,buff,size):
    """ Fonction prenant en 1er argument le fichier de log ouvert, puis en second les entrées dans une chaine de caractère et les écrits dans un fichier"""
    cPonter=ARRAY(c_uint16,size) # on declare les structures de type C
    c=cPonter()
    datePointer=ARRAY(c_float,size)
    date=datePointer()
    pathPointer=ARRAY(c_char,len(logfile))
    path=pathPointer()
    for i in range(len(logfile)): # on trasnforme le path en char*
        path[i]=c_char(logfile[i].encode("utf_8"))
    for i in range(0,size):
        chaine=buff.pop()
        d=chaine.split(':') # on separe les dates des booleens
        #print(str(d))
        date[i]=c_float(float(d[0]))
        bits=d[1]
        mask=0b10000000000000 # masque qui permet de set les bits correctement
        c1=0b00000000000000 # on initialise à 0
        for j in range(0,14):
            if(bits[j] == '1'):
                c1= c1^mask # si on a un 1, on xor avec le masque pour le rajouter au short int
            mask = mask >> 1    # on décale le masque
        #print("c1 = "+str(bin(c1)))
        c[i]=c_uint16(c1)
        #print(str(bin(c[i])))
    return IOlib.writeInput(path,date,c,size) # on ecrit tout en binaire


def ReadInputLog(logfile,Ddate,Dbits,size): # recupère la totalité des logs d'un fichier d'input
    pathPointer=ARRAY(c_char,len(logfile))
    path=pathPointer()
    for i in range(len(logfile)):
        path[i]=c_char(logfile[i].encode("utf_8"))
    size=IOlib.sizeoffile(path,0) # on recupere la taille du fichier
    #print("size = "+str(size))
    cPonter=ARRAY(c_uint16,size) # on alloue les buffeurs pour avoir la bonne taille
    c=cPonter()
    datePointer=ARRAY(c_float,size)
    date=datePointer()
    IOlib.readInput(path,date,c,size)
    for i in range(0,size):
        Ddate.append(float(date[i]))
        Dbits.append(c[i])
    return size
        #print("Date = "+str(Ddate[i]))
        #print("bits = "+str(bin(Dbits[i]))+" = "+str(Dbits[i]) )



def WriteOutputLog(logfile,buff,size):
    """ Fonction prenant en 1er argument le chemin du fichier log, puis en second les entrées dans une chaine de caractère et les écrits dans un fichier"""
    cPonter=ARRAY(c_uint16,size) # on declare les structures de type C
    c=cPonter()
    datePointer=ARRAY(c_float,size)
    date=datePointer()
    pathPointer=ARRAY(c_char,len(logfile))
    path=pathPointer()
    oiPointer=ARRAY(c_int,size)
    oi=oiPointer()
    for i in range(len(logfile)): # on trasnforme le path en char* 
        path[i]=c_char(logfile[i].encode("utf_8"))
    for i in range(size):
        chaine=buff.pop()
        d=chaine.split(':') # on separe les dates des booleens des int
        #print(str(d))
        date[i]=c_float(float(d[0]))
        oi[i]=c_int(int(d[2]))
        bits=d[1]
        mask=0b10000000000000 # masque qui permet de set les bits correctement
        c1=0b00000000000000 # on initialise à 0
        for j in range(0,11):
            if(bits[j] == '1'):
                c1= c1^mask # si on a un 1, on xor avec le masque pour le rajouter au short int
            mask = mask >> 1    # on décale le masque
        #print("c1 = "+str(bin(c1)))
        c[i]=c_uint16(c1)
        #print(str(bin(c[i])))
    return IOlib.writeOutput(path,date,c,oi,size) # on ecrit tout en binaire




def ReadOutputLog(logfile,Ddate,Dbits,Doi,size): # recupère la totalité des logs d'un fichier d'input
    pathPointer=ARRAY(c_char,len(logfile))
    path=pathPointer()
    for i in range(len(logfile)):
        path[i]=c_char(logfile[i].encode("utf_8"))
    size=IOlib.sizeoffile(path,1) # on recupere la taille du fichier
    #print("size = "+str(size))
    cPonter=ARRAY(c_uint16,size) # on alloue les buffeurs pour avoir la bonne taille
    c=cPonter()
    datePointer=ARRAY(c_float,size)
    date=datePointer()
    oiPointer=ARRAY(c_int,size)
    oi=oiPointer()
    IOlib.readOutput(path,date,c,oi,size)
    for i in range(size):
        Ddate.append(float(date[i]))
        Dbits.append(c[i])
        Doi.append(int(oi[i]))
        #print("Date = "+str(Ddate[i]))
        #print("bits = "+str(bin(Dbits[i]))+" = "+str(Dbits[i]) )


