#include <stdio.h>
#include <stdlib.h>

struct loginput{
    float date;
    short bits;
};
typedef struct loginput loginput;

struct logoutput{
    float date;
    short bits;
    int oi;
};
typedef struct logoutput logoutput;

int sizeoffile(char* path,int t)
{
    FILE* f;
    int s;
    if(f = fopen(path,"rb"))
    {
        fseek(f,0,SEEK_END);
        if(t ==0)
        {   
            s=(ftell(f)/sizeof(loginput));
        }
        else
        {
            s=(ftell(f)/sizeof(logoutput));
        }
        fclose(f);
        return s;
    }
    else
    {
        return -1;
    }

}



int readInput(char* path, float* date, unsigned short *bits, int size)
{
    int i;
    FILE* f;
    loginput* buff;
    buff=malloc(size*sizeof(loginput));
    if(f=fopen(path,"rb"))
    {
        if(fread(buff,sizeof(loginput),size,f) > 0)
        {
            for(i=0;i<size;i++)
            {
                date[i]=buff[i].date;
                bits[i]=buff[i].bits;
            }
            fclose(f);
            return 1; 
        }
        else
        {
            fclose(f);
            return -1;
        }
    }
    else
    {
        return -1;
    }
}

int writeInput(char* path, float* date, unsigned short* bits, int size)
{
    int i;
    FILE* f;
    loginput* buff;
    buff=malloc(size*sizeof(loginput));
    if(f=fopen(path,"ab"))
    {
        for(i=0;i<size;i++)
        {
            buff[i].date=date[i];
            buff[i].bits=bits[i];
        }
        if(fwrite(buff,sizeof(loginput),size,f) > 0)
        {
            fclose(f);
            return 1;
        }
        fclose(f);
        return -1;
    }
}


int readOutput(char* path, float* date, unsigned short *bits,int * oi ,int size)
{
    int i;
    FILE* f;
    logoutput* buff;
    buff=malloc(size*sizeof(logoutput));
    if(f=fopen(path,"rb"))
    {
        if(fread(buff,sizeof(logoutput),size,f) > 0)
        {
            for(i=0;i<size;i++)
            {
                date[i]=buff[i].date;
                bits[i]=buff[i].bits;
                oi[i]=buff[i].oi;
            }
            fclose(f);
            return 1;
        }
        else
        {
            fclose(f);
            return -1;
        }
    }
    else
    {
        return -1;
    }
}

int writeOutput(char* path, float* date, unsigned short* bits,int* oi ,int size)
{
    int i;
    FILE* f;
    logoutput* buff;
    buff=malloc(size*sizeof(logoutput));
    if(f=fopen(path,"ab"))
    {
        for(i=0;i<size;i++)
        {
            buff[i].date=date[i];
            buff[i].bits=bits[i];
            buff[i].oi=oi[i];
        }
        if(fwrite(buff,sizeof(logoutput),size,f) > 0)
        {
            fclose(f);
            return 1;
        }
        fclose(f);
        return -1;
    }
}