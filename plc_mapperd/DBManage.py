from DBConnect import Connect

def getActivesGroups():
	connection = Connect()
	cursor = connection.cursor()

	cursor.execute("SELECT group_id FROM Groups_MapperDemon")

	identifiers = []
	results = cursor.fetchall()
	for result in results: identifiers.append(result[0])

	cursor.close()
	connection.close()

	return identifiers


def getMapping(group_id):
        connection = Connect()
        cursor = connection.cursor()

        cursor.execute("SELECT hardware,software,direction FROM Pin_MapperDemon WHERE group_id = %s",[group_id])

        identifiers = []
        results = cursor.fetchall()
        for result in results: identifiers.append(result)

        cursor.close()
        connection.close()

        return identifiers

def getPlcs(group_id):
        connection = Connect()
        cursor = connection.cursor()

        cursor.execute("SELECT identifier,trust_factor FROM PLC_MapperDemon WHERE group_id = %s",[group_id])

        identifiers = []
        results = cursor.fetchall()
        for result in results: identifiers.append(result)

        cursor.close()
        connection.close()

        return identifiers

