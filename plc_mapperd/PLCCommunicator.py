import docker
from jsonrpcclient.http_client import HTTPClient
from jsonrpcclient.request import Request
from time import time

buffInputSet = {}

def GetAdress(identifier):
	client=docker.from_env()
	c = client.containers.list(True,filters={'id': identifier})
	if len(c) == 0: return ""
	details = client.api.inspect_container(identifier)
	return details['NetworkSettings']['IPAddress']

def GetPlcOutPins(identifier):
	#Get address
	address = GetAdress(identifier)

	#New jsonrpc batch request list
	requests = []

	#Get saved input
	in_buffered = buffInputSet.get(identifier)
	if in_buffered != None:
		for index in buffInputSet[identifier]:
			requests.append(Request('set_bool_input',index=index,value=buffInputSet[identifier][index]))

	buffInputSet[identifier] = {}

	requests.append(Request('get_outputs'))

	#Send to PLC
	client = HTTPClient('http://'+address)
	out_bools,out_int = client.send(requests)[-1]["result"]

	#Conversion for graph analysis
	mask=0b10000000000000 # masque qui permet de set les bits correctement
	c1=0b00000000000000 # on initialise à 0
	for j in out_bools:
		if j: c1+=mask # si on a un 1, on xor avec le masque pour le rajouter au short int
		mask = mask >> 1    # on décale le masque

	return str(out_int)+":"+str(int(c1))

def SetPlcInPins(identifier,pin,value):
	#Save in buffer for later request
	if buffInputSet.get(identifier) == None: buffInputSet[identifier] = {}
	buffInputSet[identifier][pin]=value
