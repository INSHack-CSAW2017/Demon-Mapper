#!/usr/bin/python3
from flask import Flask,request
app = Flask(__name__)
from threading import Thread

hard_inputs = {}
hard_outputs = {}

class gpio3:
    """ classe pour gerer facilement les GPIOs """

    def __init__(self):
        for i in range(25):
            hard_inputs[i] = False

        return

    def readInputs(self):
        """ 
        lecture de tous les pins 
        retourne une chaine de caractere avec 1 si HIGH, 0 si LOW de tous les pins d'un channel
        """
        result = ""
        for i in range(25):
            if hard_inputs[i]: result += "1"
            else: result += "0"
        return result


    def setOutput(self, pin, val):
        hard_outputs[pin] = val
        return

    def setPWM(self, pin, val):
        if pin != (12 or 13):
            return 
        else:
            hard_outputs[pin] = val
        return

@app.route("/get_inputs")
def get_inputs():
    return str(hard_inputs)

@app.route("/set_input")
def set_input():
    pin = request.args.get('pin', type = int)
    value = request.args.get('value', type = int) == 1
    hard_inputs[pin] = value
    return "Setted pin: "+str(pin)+" Value: "+str(value)

@app.route("/get_outputs")
def get_outputs():
    return str(hard_outputs)

def AppThread():
    app.run(host='127.0.0.1', port=8085)

thread = Thread(target = AppThread)
thread.start()