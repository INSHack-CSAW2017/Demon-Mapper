from LibGPIO import *
from threading import Thread,Lock
import time

myRpi = gpio3()
mutex = Lock()
virtualPins = [False for i in range(28)]

def getHardwareInPins():
        mutex.acquire()
        pins = myRpi.readInputs()
        mutex.release()
        inputs = []
        for c in pins:
                if c == "0":inputs.append(False)
                else:inputs.append(True)
        return inputs

def setHardwareOutPin(hardware_pin,value):
        if hardware_pin > 26 or hardware_pin < 2: return
        if hardware_pin == 12 or hardware_pin == 13:
                #Force int for int output
                mutex.acquire()
                myRpi.SetPwm(int(hardware_pin),value)
                mutex.release()
        else:
                mutex.acquire()
                virtualPins[hardware_pin] = (value==True)
                mutex.release()

def PinWorker():
	while True:
		try:
			for pin in range(2,28):
				mutex.acquire()
				if virtualPins[pin]:myRpi.setOutput(pin,virtualPins[pin])
				mutex.release()
		except:
			pass
		time.sleep(0.005)

def startPinWorker():
	thread = Thread(target = PinWorker)
	thread.start()