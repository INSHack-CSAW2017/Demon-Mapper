#!/usr/bin/python3.5
import liblogger
from liblogger import *
from operator import *

def genInputGraphe(logfile,graphe):
    """Cette fonction prend en argument le chemin vers un fichier de log de type input et génère un graphe de transition """
    date=[]
    bits=[]
    size=0
    Tc={}
    c=ReadInputLog(logfile,date,bits,size)
    for i in range(len(bits)-1): # on compte le nombre de chaque transition et on definit le total par ligne
        champ=str(bits[i])+','+str(bits[i+1])
        ch2=str(bits[i])
        if ch2 in Tc.keys():
            Tc[ch2]+=1
        else:
            Tc[ch2]=1
        if champ in graphe.keys():
            graphe[champ]+=1
        else:
            graphe[champ]=1
    kg=graphe.keys()
    dicf={}
    for i in kg: # on associe toutes les etats futurs de chaque etats
        subtab=[]
        d=str(i).split(',')
        if d[0] in dicf.keys():
            dicf[d[0]].append(i)
        else:
            subtab.append(i)
            dicf[d[0]]=subtab
    for k in dicf: # on calcul la proba
        for k2 in dicf[k]:
            graphe[k2]=graphe[k2]/Tc[k]

def genOutputGraphe(logfile,graphe):
    """Cette fonction prend en argument le chemin vers un fichier de log de type output et génère un graphe de transition """
    date=[]
    bits=[]
    oi=[]
    size=0
    Tc={}
    c=ReadOutputLog(logfile,date,bits,oi,size)
    for i in range(len(bits)-1): # on compte le nombre de chaque transition et on definit le total par ligne
        champ=str(oi[i])+":"+str(bits[i])+','+str(oi[i+1])+":"+str(bits[i+1])
        ch2=str(oi[i])+':'+str(bits[i])
        if ch2 in Tc.keys():
            Tc[ch2]+=1
        else:
            Tc[ch2]=1
        if champ in graphe.keys():
            graphe[champ]+=1
        else:
            graphe[champ]=1
    kg=graphe.keys()
    dicf={}
    for i in kg: # on associe toutes les etats futurs de chaque etats
        subtab=[]
        d=str(i).split(',')
        if d[0] in dicf.keys():
            dicf[d[0]].append(i)
        else:
            subtab.append(i)
            dicf[d[0]]=subtab
    for k in dicf: # on calcul la proba
        for k2 in dicf[k]:
            graphe[k2]=graphe[k2]/Tc[k]



def gendot(file,graphe,name): 
    """ Fonction permettant de transformer un graphe sous forme de dictionnaire en un code dot pouvant etre compile pour obtenir une image etc"""
    code="Digraph "+name+"{"
    f = open(file,"w")
    f.write(code)
    for i in graphe.keys():
        d=str(i).split(',')
        code="  "+d[0]+" -> "+d[1]+"[label="+str(graphe[i])+"];"
        f.write(code)
    f.write("}")


def genTree(graphe,tree):
    """Fonction permettant de générer un dictionnaire, ou chaque clé correspond à un état. A chaque clé est associé un tableau de dictionnaire. Chaque clé de dictionnaire est un etat fils et chaque valeur est la proba d'arriver sur cet etat """
    for k in graphe.keys(): # pour chaque clé du dictionnaire
        sub=[]  
        subp={}
        d=str(k).split(',') # on sépare la clé pour récupérer l'etat de départ
        if d[0] in tree.keys(): #si la clé existe déjà, on rajoute l'etat fils avec la proba 
            subp[d[1]]=graphe[k]
            tree[d[0]].append(subp)
        else:                      # si la clé n'existe pas, on la crée et on met un tableau de dictionnaire en valeur
            subp[d[1]]=graphe[k]
            sub.append(subp)
            tree[d[0]]=sub


def getSucc(tree,state,succ):
    if state in tree.keys():
        k=tree[state]
        for i in range(len(k)):
            d=str(k[i].keys()).split('\'')
            dic=k[i]
            succ[d[1]]=dic[d[1]]


def grapheCompare(grapheControl,grapheTest,arg):
    """ Fonction ayant pour but de comparer deux graphes entre eux afin de détecter la corruption de l'un par rapport à l'autre. La fonction prend
    en argument un graphe de controle, un graphe à controler, un dictionnaire d'arguments contenant le trust_coeff et un str qui contiendra les message de logs """
    message="CONTROL BEGENNING:\n"
    trust_coeff=arg['trust_coeff']
    for k in grapheTest: # pour chaque transition dans le graphe à controller
        print(str(k))
        print(str(grapheControl.keys()))
        if k in grapheControl.keys():   # si la transition est définie dans le graphe de controle, alors
            if grapheControl[k] == -1:  # si la transition est interdite, le PLC est corrompu
                trust_coeff=-1
                message="CORRUPTED, PLC REACHED FORBBIDEN STATE "+str(k)
                arg['trust_coeff']=trust_coeff
                arg['message']=message
                return message
            else:
                if (grapheTest[k] > grapheControl[k]*0.9) and (grapheTest[k] < grapheControl[k]*1.1): # on accepte une marge d'erreur de 10% par rapport aux proba de base
                    message+=" Transition "+str(k)+" in the 10% interval, trust factor up\n"
                    if trust_coeff+0.01 <=1:
                        trust_coeff+=0.01
                else:
                    if(grapheTest[k] > grapheControl[k]*0.65) and (grapheTest[k] < grapheControl[k]+1.35):  # si la différence de proba avec la transition du graphe originel est entre 10 et 35%
                        message+=" Transition "+str(k)+" in the 35% interval, trust factor down by 0.015\n"
                        if trust_coeff-0.025 > 0:
                            trust_coeff-=0.025
                        else:
                            message+=" Trust factor < 0 ==> CORRUPTED\n"
                            trust_coeff = -1
                            arg['trust_coeff']=trust_coeff
                            arg['message']=message
                            return -1
                    else :
                        if(grapheTest[k] > grapheControl[k]*0.35) and (grapheTest[k] < grapheControl[k]+1.65): # si on est dasn l'intervale 35-65% de différence
                            message+=" Transition "+str(k)+" in the 65% interval, trust factor down by 0.2\n"
                            if trust_coeff-0.2 > 0:
                                trust_coeff-=0.2
                            else:                                                                         
                                message+=" Trust factor < 0 ==> CORRUPTED\n"
                                trust_coeff = -1
                                arg['trust_coeff']=trust_coeff
                                arg['message']=message
                                return -1
                        else:
                            message+=" Transition "+str(k)+" outside the 65% interval, ==> CORRUPTED\n"  # si il y a plus de 65% de différence par rapport à la valeur originelle
                            trust_coeff=-1
                            arg['trust_coeff']=trust_coeff
                            arg['message']=message
                            return -1
        else:                                                                                   # dans le cas où la transition n'est pas prévue
            message+="WARNING REACHED NO SET STATE "+str(k)+"\n"
            if grapheTest[k] > 0.1 and grapheTest[k] < 0.25:
                message+="between 10% and 25% error decrease trust factor by 0.055\n"
                if trust_coeff - 0.055 > 0:
                    trust_coeff-=0.055
                else :
                    message+=" Trust factor < 0 ==> CORRUPTED\n"
                    trust_coeff = -1
                    arg['trust_coeff']=trust_coeff
                    arg['message']=message
                    return message
            if grapheTest[k] > 0.25 and grapheTest[k] < 0.5:
                message+="between 25% and 50% error decrease trust factor by 0.25\n"
                if trust_coeff - 0.25 > 0:
                    trust_coeff-=0.25
                else :
                    message+=" Trust factor < 0 ==> CORRUPTED\n"
                    trust_coeff = -1
                    arg['trust_coeff']=trust_coeff
                    arg['message']=message
                    return -1
            if grapheTest[k] > 0.5:
                message+="over 50% error decrease trust factor by 0.75\n"
                if trust_coeff - 0.75 > 0:
                    trust_coeff-=0.75
                else :
                    message+=" Trust factor < 0 ==> CORRUPTED\n"
                    trust_coeff = -1
                    arg['trust_coeff']=trust_coeff
                    arg['message']=message
                    return -1

    message+=" END OF CONTROL\n"
    arg['trust_coeff']=trust_coeff
    arg['message']=message
    return 1


def PSuccMax(tree,cstate):
    suiv={}
    getSucc(tree,cstate,suiv)
    state=''
    pm=-1
    for k,v in suiv.items() :
        if v > pm :
            pm = v
            state = k
    return state


def decision(outputs,n,tree,cstate):
    final=" "
    state={}
    T_trust=0
    for i in range(n): # on calcule le poids de chaque réponse
        o = outputs[i]
        d=str(o.keys()).split('\'')
        if d[1] in state.keys():
            state[d[1]]+=o[d[1]]
            T_trust+=o[d[1]]
        else:
            state[d[1]]=o[d[1]]
            T_trust+=o[d[1]]
    state_trie = sorted(state.items(), key=itemgetter(1) )
    c0=state_trie[len(state_trie)-1]
    c1=state_trie[len(state_trie)-2]
    final=c0[0]
    if T_trust > n/4 :  # moyenne de trust factor > 0.25
        #state_trie = sorted(state.items(), key=itemgetter(1) )
        #c0=state_trie[len(state_trie)-1]
        #c1=state_trie[len(state_trie)-2]
        #print("c0 = "+str(c0))
        #print("c1 = "+str(c1))
        #final=c0[0]
        if c0[1] > c1[1] : # on verifie qu'il n'y a pas d'égalité
            if cstate != '-1':  # on verifie que l'on a bien une réponse valide
                #print("cstate 1 "+cstate)
                suiv={}
                getSucc(tree,cstate,suiv)
                #print("suiv = "+str(suiv))
                if final in suiv.keys():
                    #print('final dans suiv')
                    return final
                else :                  # sinon on prend le successeur ayant la plus grosse proba d'arriver
                    final=PSuccMax(tree,cstate)
                    return final
            else :                      # si on a pas d'abre de controle, on laisse l'etat choisit par le groupe
                return final
        else :
            if cstate != '-1' :             # en cas d'égalité, on prend le fils ayant la plus grande proba
                final = PSuccMax(tree,cstate)
                return final
            else :
                return final
    else :
        if cstate != '-1' :
            final=PSuccMax(tree,cstate)
            return final        
    return final


"""
print("----------  GRAPHE FINAL  ---------")
#grapheI={}
#genInputGraphe("input.log",grapheI)
#print(str(grapheI))
grapheO={}
genOutputGraphe("output.log",grapheO)
print("graphe O = "+str(grapheO))
tree={}
genTree(grapheO,tree)
print("TREE + " + str(tree))
succ={}
getSucc(tree,"44:12033",succ)
print(str(succ))
#arg={}
#arg['trust_coeff']=1
#arg['message']=" "
#c=grapheCompare(grapheI,grapheI,arg)
#print("trust coeef = "+str(arg['trust_coeff'])+"  "+arg['message'])
#arg['trust_coeff']=1
#arg['message']=" "
#c=grapheCompare(grapheI,grapheO,arg)
#print("trust coeef = "+str(arg['trust_coeff'])+"  "+arg['message']+"c = "+str(c))
output=[{'44:12033':0.45},{'44:12033':0.55},{'504:42033':0.65},{'504:42033':0.65}]
n=4
t=0
final = decision(output,n,tree,'44:12033')
print("FINAL = "+final)
"""
