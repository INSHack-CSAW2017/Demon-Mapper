#!/usr/bin/python3
import time,sys,traceback,logging,daemonize
from Logger_Layer import log_inputs,log_outputs,log_plc_output
from PLCCommunicator import GetPlcOutPins,SetPlcInPins
from DBManage import getActivesGroups,getMapping,getPlcs
from HardwareLayer import getHardwareInPins,setHardwareOutPin,startPinWorker
from graphe import decision

name = "plc_mapperd"
pid = "/var/run/"+name+".pid"

def work():
    #Remove logging from jsonrpc
    for a in ("jsonrpcclient.client.request","jsonrpcclient.client.response"):
        logging.getLogger(a).setLevel(logging.CRITICAL)

    #Create logger
    logging.basicConfig(filename='/var/log/plc_mapperd.log', level=logging.INFO)

    #Say it's started
    logging.info("Mapper started working")

    usleep = lambda x: time.sleep(x/1000000.0)

    #Start pin worker
    startPinWorker()

    while 1:

        #Get inputs
        try:
            HardwareInputs = getHardwareInPins()
        except Exception as ex:
            HardwareInputs = [ False for i in range(14)]
            logging.error('Error getting hardware inputs: '+str(ex))

        #Get groups
        try:
            groups = getActivesGroups()
        except Exception as ex:
            groups = []
            logging.error('Error getting groups: '+str(ex))

	    #For each plc group
        for group_id in groups:
            #Get pin mapping per group
            try:
                mapping = getMapping(group_id)
            except Exception as ex:
                mapping = []
                logging.error('Error getting mapping: '+str(ex))

            #Get plcs per group
            try:
                plcs = getPlcs(group_id)
            except Exception as ex:
                plcs = []
                logging.error('Error getting plcs: '+str(ex))

            #Set all plcs inputs
            inputs = [False for i in range(14)]
            for hard_pin,soft_pin,direction in mapping:
                #Input
                if direction:
                    #For loggin inputs (-2 for BCM)
                    inputs[soft_pin]=HardwareInputs[hard_pin-2]

                    #Set all plcs
                    for identifier,trust_factor in plcs:
                        try:
                            #(-2 for BCM)
                            SetPlcInPins(identifier,soft_pin,HardwareInputs[hard_pin-2])
                        except Exception as ex:
                            logging.error('Error setting pin for PLC: '+str(ex))

            #Log inputs
            try:
                log_inputs(group_id,inputs)
            except Exception as ex:
                logging.error('Error setting plcs inputs logs: '+str(ex))

            #Get all plcs outputs
            outputs = []
            for identifier,trust_factor in plcs:
                try:
                    newdico ={}
                    plc_out = GetPlcOutPins(identifier)
                    newdico[plc_out]=float(trust_factor)/100.0
                    outputs.append(newdico)
                    log_plc_output(group_id,identifier,plc_out)
                except Exception as ex:
                    logging.error('Error getting plcs output: '+str(ex))

            #Get the prediction depending on the outputs
            try:
                final = decision(outputs,len(outputs),{},'-1')
            except Exception as ex:
                final = "0:0"
                logging.error('Error making decision: '+str(ex))

            #Conversion
            try:
                final_int,final_bools = map(int,final.split(":"))
                print(final_bools)
                bool_output = []
                mask = 0b10000000000000
                for i in range(11):
                    if final_bools&mask:bool_output.append(True)
                    else: bool_output.append(False)
                    mask = mask >> 1
                output = (final_int,bool_output)

            except Exception as ex:
                output = (0,0)
                logging.error('Error parsing decision: '+str(ex))

            #Log outputs
            try:
                log_outputs(group_id,output)
            except Exception as ex:
                logging.error('Error setting plcs final group output logs: '+str(ex))

            #Set output for hardware pin
            for hard_pin,soft_pin,direction in mapping:
                if not direction:
                    for identifier,trust_factor in plcs:
                        try:
                            #Soft pin 0 => int output
                            if soft_pin == 0 and (hard_pin == 12 or hard_pin == 13):
                                setHardwareOutPin(hard_pin,final_int)
                            else:
                                setHardwareOutPin(hard_pin,bool_output[soft_pin-1])
                        except Exception as ex:
                            logging.error('Error setting final output: '+str(ex))
        usleep(5000)

daemon = daemonize.Daemonize(app=name, pid=pid, action=work)
daemon.start()
