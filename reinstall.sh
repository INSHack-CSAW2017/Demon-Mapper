#!/bin/bash

#Copy executables
cp -R plc_mapperd /usr/sbin/

#Move init file
cp init-script/plc_mapperd /etc/init.d/plc_mapperd
sudo chmod 0755 /etc/init.d/plc_mapperd

#Compile librairy
cd /usr/sbin/plc_mapperd
rm IOlib.so
gcc -c IOlib.c
gcc -shared -o IOlib.so IOlib.o
rm IOlib.o
cd ..

#Reload daemons
systemctl daemon-reload

#Add at start
sudo update-rc.d plc_mapperd defaults

sudo service plc_mapperd start
