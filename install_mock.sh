#!/bin/bash

#Make log dir
mkdir /var/log/gpioInOuts/

#Install required
sudo apt-get update && sudo apt-get install -y python3 python3-pip python3-requests mariadb-server libmariadbclient-dev
sudo pip3 install docker mysqlclient daemonize jsonrpcclient flask

#Copy executables
cp -R plc_mapperd /usr/sbin/

#Move init file
cp init-script/plc_mapperd /etc/init.d/plc_mapperd
sudo chmod 0755 /etc/init.d/plc_mapperd

#Compile librairy
cd /usr/sbin/plc_mapperd
rm IOlib.so
gcc -c IOlib.c
gcc -shared -o IOlib.so IOlib.o
rm IOlib.o

#Install mock
cp MockGPIO/LibGPIOMock.py LibGPIO.py
cd ..

#Reload daemons
systemctl daemon-reload

#Add at start
sudo update-rc.d plc_mapperd defaults

sudo service plc_mapperd start
